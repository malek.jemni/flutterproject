import 'package:flutter/material.dart';

import 'event_details.dart';
import 'home/home.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF202020),  // Set the background color here
      child: MaterialApp(
        title: 'Trash Trooper',
        routes: {
          "/": (BuildContext context) => Home(),
        },
      ),
    );
  }
}