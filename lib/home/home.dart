import 'package:flutter/material.dart';
import 'event_collect.dart';

class Home extends StatefulWidget {
  const Home();

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final List<Event> _events = [];
  final String _description = "This event is for cleaning the area and repairing the broken chairs";

  @override
  void initState() {
    Money money1 = Money(1, 500);
    Hardware hardware1 = Hardware(2, ['Keyboard', 'Mouse', 'Monitor']);
    _events.add(Event("assets/images/dirty1.jpg", "Area 1", _description, money1, hardware1, 20));
    _events.add(Event("assets/images/dirty2.jpg", "Area 2", _description, money1, hardware1, 20));
    _events.add(Event("assets/images/dirty3.jpg", "Area 3", _description, money1, hardware1,20));
    _events.add(Event("assets/images/dirty4.jpg", "Area 4", _description, money1, hardware1, 20));
    _events.add(Event("assets/images/dirty5.jpg", "Area 5", _description, money1, hardware1, 20));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: _events.length,
        itemBuilder: (BuildContext context, int index) {
          return EventCollect(_events[index]);
        }
    );
  }
}

class Event {
  String url;
  String title;
  String description;
  Money collect_money;
  Hardware collect_hardware;
  int participant;

  Event(this.url, this.title, this.description, this.collect_money,this.collect_hardware, this.participant);

  @override
  String toString() {
    return 'Event{_url: $url, _title: $title, _description: $description, _money: $collect_money, _hardware: $collect_hardware,_participant: $participant}';
  }
}

class Money{
  int user_id;
  int required_amount;

  Money(this.user_id,this.required_amount);


}
class Hardware{
  int user_id;
  List<String> hardwares;

  Hardware(this.user_id,this.hardwares);
}