import 'package:flutter/material.dart';

import '../event_details.dart';
import 'home.dart';

class EventCollect extends StatelessWidget {

  final Event _event;

  EventCollect(this._event);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        color: Color(0xFF454545), // Background color
        child: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => EventDetails(_event))
            );
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: Image.asset(_event.url, width: 100, height: 100),
              ),
              const SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _event.title,
                    textScaleFactor: 1.5,
                    style: TextStyle(color: Colors.white), // Text color
                  ),
                  Text(
                    "Funds : 0/${_event.collect_money.required_amount} TND",
                    textScaleFactor: 1,
                    style: TextStyle(color: Colors.white), // Text color
                  ),
                ],
              ),
              const SizedBox(
                width: 50,
              ),
              Icon(
                Icons.arrow_forward_ios_outlined,
                color: Colors.white, // Arrow color
                size: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
