import 'package:flutter/material.dart';
import 'home/home.dart';

class EventDetails extends StatefulWidget {
  final Event _event;

  const EventDetails(this._event);

  @override
  State<EventDetails> createState() => _EventDetailsState();
}

class _EventDetailsState extends State<EventDetails> {
  late int _currentQuantity;
  TextEditingController _moneyController = TextEditingController();
  int _donatedAmount = 0;

  @override
  void initState() {
    _currentQuantity = widget._event.participant;
    super.initState();
  }

  void _fundEvent() {
    setState(() {
      int addedAmount = int.tryParse(_moneyController.text) ?? 0;
      _donatedAmount += addedAmount;
      _moneyController.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF454545),
        title: Text(widget._event.title),
      ),
      body: Container(
        color: Color(0xFF202020), // Background color
        child: Column(
          children: [
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Image.asset(widget._event.url, width: 460, height: 215),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 50),
              child: Text(widget._event.description,
                style: TextStyle(color: Colors.white),),
            ),
            Text("$_donatedAmount/${widget._event.collect_money.required_amount} TND",
              style: TextStyle(color: Colors.white),
               textScaleFactor: 3),
            Text("Participants : $_currentQuantity",
                style: TextStyle(color: Colors.white)),
            const SizedBox(height: 10),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child:  TextField(
                controller: _moneyController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Enter amount to fund'),
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 50),
              child:
              ElevatedButton(
                onPressed: _fundEvent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: [
                    Icon(Icons.monetization_on),
                    SizedBox(width: 5),
                    Text("Fund", textScaleFactor: 1.5),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
